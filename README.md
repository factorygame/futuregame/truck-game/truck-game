# Truck Game

Driving a truck to make deliveries around the city

## Overview

The player has to drive his truck to the factory to collect a payload,
then drive to the destination and offload cargo.

## Installation

Just download it and start working straight away. It is recommended to have
some Python knowledge, although this is optional. First take a look at the
included samples by executing `run_test.py`. This will showcase all the current
included features.
