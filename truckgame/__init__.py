"""Driving a truck to make deliveries around the city.

The player has to drive his truck to the factory to collect a payload,
then drive to the destination and offload cargo.
"""

from truckgame.core.truckgame_engine import TruckGameEngine
