from factorygame import GameEngine
from factorygame.core.input_base import EKeys
from truckgame.maps.main_menu import TruckMainMenuWorld

class TruckGameEngine(GameEngine):
    def __init__(self):
        super().__init__()
        self._starting_world = TruckMainMenuWorld
        self._window_title = "Garbage Truck Simulator 2019"
        self._frame_rate = 60

    def setup_input_mappings(self):
        # Control vehicle
        self.input_mappings.add_action_mapping("SideViewAccelerate", EKeys.D)
        self.input_mappings.add_action_mapping("SideViewBrake", EKeys.A)

        self.input_mappings.add_action_mapping("TopDownAccelerate", EKeys.W)
        self.input_mappings.add_action_mapping("TopDownBrake", EKeys.S)
        self.input_mappings.add_action_mapping("TopDownSteerLeft", EKeys.A)
        self.input_mappings.add_action_mapping("TopDownSteerRight", EKeys.D)

        self.input_mappings.add_action_mapping("Horn", EKeys.H)
        self.input_mappings.add_action_mapping("EngineToggle", EKeys.M)

        # World map view
        self.input_mappings.add_action_mapping("ConfirmAction", EKeys.F)
