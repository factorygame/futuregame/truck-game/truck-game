"""Defines how a vehicle (truck) should move.
"""

from math import pi

from factorygame import Loc, MathStat, GameplayStatics
from factorygame.core.blueprint import PolygonNode, GeomHelper
from factorygame.core.input_base import EInputEvent


class VehicleBase(PolygonNode):
    """Vehicle base class with acceleration and braking.
    """

    def __init__(self):
        super().__init__()

        # X is forward accelerate, Y is steer right
        self.speed = Loc(0, 0)
        self.acceleration = Loc(0, 0)
        # Clockwise rotation from north line, in radians
        self.rotation = 0.0

        self.speed_input = Loc(0, 0)
        self.brake_input = Loc(0, 0)
        self.steer_right_input = 0.0

        self.acceleration_rate = 0.7
        self.reverse_acceleration_rate = 0.60

    def begin_play(self):
        super().begin_play()
        self.vertices = list(GeomHelper.generate_reg_poly(4, radius=30))

        self.setup_input_component(GameplayStatics.game_engine.input_mappings)

    def setup_input_component(self, input_component):
        """Override in children bind accelerate and brake input.
        """

    # Input Callbacks (must be bound by children)

    def on_accelerate_pressed(self):
        self.speed_input.x = 1
    def on_accelerate_released(self):
        self.speed_input.x = 0

    def on_brake_pressed(self):
        self.brake_input.x = 1.0
    def on_brake_released(self):
        self.brake_input.x = 0.0

    def on_steer_right_pressed(self):
        self.steer_right_input = 1
    def on_steer_right_released(self):
        if self.steer_right_input == 1:
            self.steer_right_input = 0

    def on_steer_left_pressed(self):
        self.steer_right_input = -1
    def on_steer_left_released(self):
        if self.steer_right_input == -1:
            self.steer_right_input = 0

    def tick_physics(self, dt):
        """Calculate speed change (must be called each frame manually)
        """

        dt *= 0.001

        # Add rotation
        rotation_add_amount = self.steer_right_input * dt * 2 * MathStat.lerp(0, 1, abs(self.speed)/2) #* (1 if abs(self.speed) > 0 else -1)
        self.rotation += rotation_add_amount
        rotation_direction = tuple(GeomHelper.generate_reg_poly(
            1, radial_offset=(2 * pi) - self.rotation))[0]
        speed_change_magnitude = abs(self.speed_input) * self.acceleration_rate

        if speed_change_magnitude <= 0.5:
            # Acceleration nearly zero. Engage reverse.
            speed_change_magnitude -= abs(-self.brake_input) * self.reverse_acceleration_rate
            if speed_change_magnitude < -0.5:
                # We're already moving in reverse
                self.rotation -= rotation_add_amount * 2
        elif self.brake_input.x > 0:
            # Decelerate with braking while moving forward.
            self.speed *= 0.7 #* dt * 25
            pass

        target_speed_change = rotation_direction * speed_change_magnitude
        new_speed_change = MathStat.lerp(self.acceleration, target_speed_change, 0.1)
        self.acceleration = new_speed_change
        self.speed += new_speed_change
        #print(speed_change.y + self.speed.y)

        if not -0.5 < abs(self.speed) < 0.5:
            # Decelerate naturally.
            self.speed *= 0.95 #* min(dt * 35, 1)
            pass
        else:
            # Clamp speed to exactly zero if near.
            self.speed = Loc(0, 0)


class TopDownVehicle(VehicleBase):
    """Vehicle that is viewed from above and can turn.
    """

    def __init__(self):
        super().__init__()
        self.rotation = -pi / 2

    def setup_input_component(self, input_component):
        # Control vehicle
        input_component.bind_action("TopDownAccelerate",
                                    EInputEvent.PRESSED,
                                    self.on_accelerate_pressed)
        input_component.bind_action("TopDownAccelerate",
                                    EInputEvent.RELEASED,
                                    self.on_accelerate_released)

        input_component.bind_action("TopDownBrake",
                                    EInputEvent.PRESSED,
                                    self.on_brake_pressed)
        input_component.bind_action("TopDownBrake",
                                    EInputEvent.RELEASED,
                                    self.on_brake_released)

        input_component.bind_action("TopDownSteerRight",
                                    EInputEvent.PRESSED,
                                    self.on_steer_right_pressed)
        input_component.bind_action("TopDownSteerRight",
                                    EInputEvent.RELEASED,
                                    self.on_steer_right_released)

        input_component.bind_action("TopDownSteerLeft",
                                    EInputEvent.PRESSED,
                                    self.on_steer_left_pressed)
        input_component.bind_action("TopDownSteerLeft",
                                    EInputEvent.RELEASED,
                                    self.on_steer_left_released)

    def tick(self, dt):
        self.tick_physics(dt)
        self.set_verts_from_rotation()
        delta_seconds = dt * 0.001

        # Switch y and x axis
        speed = Loc(self.speed.y, self.speed.x)

        # Interpolate to target position smoothly
        target = self.location + speed * delta_seconds * 100
        self.location = MathStat.lerp(self.location, target, 0.4)

        if not self.world.allow_world_drag_navigation:
            # Camera to track player location if not adjusting manually.
            self.world._view_offset = self.location

        # Display current shape.
        super().tick(dt)

    def set_verts_from_rotation(self):
        self.vertices = list(
            GeomHelper.generate_reg_poly(4, radius=30, radial_offset=self.rotation))


class SideViewVehicle(VehicleBase):
    """Vehicle that is viewed from its right side and can't turn.

    Viewed from right side means the truck's front end is facing
    towards the right of the viewport.
    """

    def setup_input_component(self, input_component):
        # Control vehicle
        input_component.bind_action("SideViewAccelerate",
                                    EInputEvent.PRESSED,
                                    self.on_accelerate_pressed)
        input_component.bind_action("SideViewAccelerate",
                                    EInputEvent.RELEASED,
                                    self.on_accelerate_released)

        input_component.bind_action("SideViewBrake",
                                    EInputEvent.PRESSED,
                                    self.on_brake_pressed)
        input_component.bind_action("SideViewBrake",
                                    EInputEvent.RELEASED,
                                    self.on_brake_released)

    def tick(self, dt):
        self.tick_physics(dt)
        delta_seconds = dt * 0.001

        # Switch y and x axis
        speed = Loc(self.speed.y, self.speed.x)

        # Interpolate to target position smoothly
        target = self.location + speed * delta_seconds * 100
        self.location = MathStat.lerp(self.location, target, 0.4)

        if not self.world.allow_world_drag_navigation:
            # Camera to track player location if not adjusting manually.
            self.world._view_offset = self.location

        # Display current shape.
        super().tick(dt)
