from factorygame import Loc, FColor, MathStat, GameplayStatics, GameplayUtilities
from factorygame.core.engine_base import ETickGroup
from factorygame.core.blueprint import PolygonNode, NodeBase
from factorygame.core.input_base import EInputEvent
from truckgame.core.vehicle_base import VehicleBase


dock_speed = 0.1


class WasteChuteBase(PolygonNode):
    """Visible chute that dumps garbage into a truck.
    """

    def begin_play(self):
        self.vertices = [Loc(-1.8367346938778155, 280.2857142857118), Loc(192.8571428571426, 286.1632653061199), Loc(202.40816326530586, 77.5102040816302), Loc(123.06122448979562, 79.71428571428328), Loc(122.3265306122446, 195.79591836734448), Loc(106.16326530612213, 222.97959183673225), Loc(-1.8367346938778155, 218.57142857142605)]
        self.fill_color = FColor(189, 184, 179)


class FallingTrashStream(PolygonNode):
    """Visible stream of trash coming from waste chute
    """
    def __init__(self):
        super().__init__()
        self.primary_actor_tick.tick_group = ETickGroup.WORLD
        self.generate_click_events = False
        self.generate_cursor_over_events = False

    def begin_play(self):
        self.vertices_frames = [
            [Loc(135.55102040816348, 77.5102040816372), Loc(178.89795918367366, 81.9183673469434), Loc(200.20408163265324, 12.857142857147494), Loc(224.44897959183692, -63.55102040815865), Loc(197.2653061224492, -87.79591836734232), Loc(195.06122448979607, -127.46938775509739), Loc(100.28571428571445, -115.71428571428106), Loc(128.2040816326533, -76.77551020407702), Loc(96.61224489795939, -9.91836734693419), Loc(120.85714285714306, 35.632653061229064)],
            [Loc(115.71428571428669, 75.30612244898344), Loc(179.63265306122545, 74.57142857143248), Loc(189.18367346938874, 9.183673469391636), Loc(213.4285714285724, -59.87755102040427), Loc(220.04081632653157, -120.12244897958789), Loc(197.26530612244994, -175.95918367346547), Loc(135.55102040816425, -167.1428571428532), Loc(70.16326530612344, -161.26530612244505), Loc(92.20408163265404, -95.14285714285325), Loc(120.85714285714383, -23.87755102040427), Loc(113.51020408163362, 28.2857142857182)]
            ]

        self.vertices = self.vertices_frames[0]
        self.fill_color = FColor(122, 75, 10)
        self.total_time = 0.0

    def tick(self, dt):
        self.total_time += dt * 0.001
        interval = self.total_time % 0.5

        frame_index = 0
        if interval < 0.2:
            frame_index = 0
        elif interval < 0.4:
            frame_index = 1

        self.vertices = self.vertices_frames[frame_index]
        super().tick(dt)

class TruckDockBase(PolygonNode):
    """Area the truck can enter to perform some action.
    """

    def __init__(self):
        super().__init__()

        self.primary_actor_tick.tick_group = ETickGroup.UI
        self.generate_click_events = False
        # Necessary to allow multi_box_trace to see it.
        # TODO: Should be set in a different flag!
        self.generate_cursor_over_events = True

        self.length = 300.0
        self.height = 30.0
        self.fill_color = FColor.yellow()
        self.should_visualise_area = True

        self.light_bias = 0.0
        self.light_direction = 1

        self.is_truck_inside = False
        # Must be inside to be stationary
        self.is_truck_stationary = False

    def begin_play(self):
        super().begin_play()

        # Draw a box to visualise collision area

        half_size = Loc(self.length / 2, self.height / 2)
        c1 = half_size
        c2 = -half_size
        self.vertices = (
            c1, Loc(c1.x, c2.y),
            c2, Loc(c2.x, c1.y)
            )

    def _should_draw(self):
        return self.should_visualise_area and super()._should_draw()

    def tick(self, dt):
        # Change flashing light color.
        self.light_bias += dt * 0.001 * self.light_direction * 2
        if not 0 < self.light_bias < 1:
            self.light_direction *= -1
        self.fill_color = MathStat.lerp(FColor.yellow(), FColor.white(), self.light_bias)

        # Check if player's inside
        self.check_player_collision()

        super().tick(dt)

    def check_player_collision(self):
        """Check if player is inside to invoke docking functions.
        """

        # Of course, screen space collisions only!
        start = self.world.view_to_canvas(self.location)

        view_half_size = Loc(self.length / 2, self.height / 2)
        c1 = self.world.view_to_canvas(self.location + view_half_size)
        c2 = self.world.view_to_canvas(self.location - view_half_size)
        half_size = (c2 - c1) / 2

        found = []
        self.world.multi_box_trace_for_objects(start, half_size, found)
        truck, *_ = tuple(filter(
                lambda actor: isinstance(actor, VehicleBase), found)) or (None,)

        if truck is not None:
            if not self.is_truck_inside:
                # Truck just entered for the first time.
                self.is_truck_inside = True
                self.on_truck_enter(truck)

            elif not self.is_truck_stationary and abs(truck.speed) == 0:
                # Truck is stationary for the first time.
                self.is_truck_stationary = True
                self.on_truck_begin_stationary(truck)

            elif self.is_truck_stationary and abs(truck.speed) != 0:
                # Truck was stationary but isn't anymore.
                self.is_truck_stationary = False
                self.on_truck_end_stationary(truck)

        elif self.is_truck_inside:
            # Truck left but was inside previously.
            self.is_truck_inside = False
            self.on_truck_leave(truck)

    def on_truck_enter(self, truck):
        """Called when truck enters the zone.
        """
        print("on_truck_enter")

    def on_truck_leave(self, truck):
        """Called when truck leaves the zone.
        """
        print("on_truck_leave")

    def on_truck_begin_stationary(self, truck):
        """Called when truck has entered the zone and is now stopped.
        """
        print("on_truck_begin_stationary")

    def on_truck_end_stationary(self, truck):
        """Called when truck was stopped in the zone and is now moving.
        """
        print("on_truck_end_stationary")


class TruckLoadingDock(TruckDockBase):
    """Loads truck with garbage.
    """

    def __init__(self):
        super().__init__()

        self.is_loaded = False
        self.can_load = False
        self.cargo_full_percent = 0.0
        self.trash_stream = None

    def begin_play(self):
        super().begin_play()

        # Check if we're in the right mission area.
        self.can_load = self.world.is_mission_area()
        if self.can_load:
            # In the correct mission area.
            self.please_enter_zone_msg = "Enter flashing area (left) to start loading"
            self.please_stop_msg = "Remain stationary to start loading"
            self.please_wait_loading_msg = "Loading truck. Please wait"
            self.please_leave_area_msg = "Truck loaded. Go to world"
        else:
            # Incorrect mission area.
            self.should_visualise_area = False
            self.please_enter_zone_msg = "Wrong mission area"
            self.please_stop_msg = "Wrong mission area"
            self.please_wait_loading_msg = "Wrong mission area"
            self.please_leave_area_msg = "Wrong mission area"

        self.world.hint_text = self.please_enter_zone_msg

    def on_truck_enter(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_stop_msg

    def on_truck_leave(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_enter_zone_msg
        else:
            self.world.hint_text = self.please_leave_area_msg

    def on_truck_begin_stationary(self, truck):
        if self.can_load:
            if self.is_loaded:
                self.world.hint_text = "Already loaded"

            else:
                self.trash_stream = self.world.spawn_actor(
                    FallingTrashStream, self.world.waste_chute.location)

                self.world.hint_text = "Loading truck. Please wait"


    def on_truck_end_stationary(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_stop_msg
            if self.trash_stream is not None:
                self.world.destroy_actor(self.trash_stream)

    def tick(self, dt):
        if self.can_load and self.is_truck_stationary and not self.is_loaded:
            # Increment player fullness.
            self.cargo_full_percent += dt * 0.001 * dock_speed

            if self.cargo_full_percent >= 1.0:
                # Reached full cargo load.
                self.cargo_full_percent = 1.0
                self.world.hint_text = self.please_leave_area_msg
                self.is_loaded = True
                self.should_visualise_area = False
                if self.trash_stream is not None:
                    self.world.destroy_actor(self.trash_stream)

                player_position = self.world.player.location
                self.world.destroy_actor(self.world.player)

                loaded_truck_class = GameplayStatics.persistent_world_cache["vehicle_class_side_loaded"]
                GameplayStatics.persistent_world_cache["mission_has_loaded"] = True
                self.world.player = self.world.spawn_actor(
                    loaded_truck_class, player_position)

            else:
                # Still filling
                self.world.hint_text = "Loading truck %.2f%%. Please wait" % (
                    self.cargo_full_percent * 100)

        super().tick(dt)


class TruckOffloadingDock(TruckDockBase):
    """Loads truck with garbage.
    """

    def __init__(self):
        super().__init__()

        self.is_loaded = False
        self.can_load = False
        self.cargo_full_percent = 1.0
        self.trash_stream = None


    def begin_play(self):
        super().begin_play()

        # Check if we're in the right mission area.
        self.can_load = self.world.is_mission_area()
        if self.can_load:
            # In the correct mission area.
            self.please_enter_zone_msg = "Enter flashing area to start offloading"
            self.please_stop_msg = "Remain stationary to start offloading"
            self.please_wait_loading_msg = "Offloading truck. Please wait"
            self.please_leave_area_msg = "Truck offloaded. Go to world"
        else:
            # Incorrect mission area.
            self.should_visualise_area = False
            self.please_enter_zone_msg = "Wrong mission area"
            self.please_stop_msg = "Wrong mission area"
            self.please_wait_loading_msg = "Wrong mission area"
            self.please_leave_area_msg = "Wrong mission area"

        self.world.hint_text = self.please_enter_zone_msg

    def on_truck_enter(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_stop_msg

    def on_truck_leave(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_enter_zone_msg
        else:
            self.world.hint_text = self.please_leave_area_msg

    def on_truck_begin_stationary(self, truck):
        if self.can_load:
            if self.is_loaded:
                self.world.hint_text = "Already offloaded"

            else:
                self.trash_stream = self.world.spawn_actor(
                    FallingTrashStream, self.world.waste_chute.location)

                self.world.hint_text = "Offloading truck. Please wait"


    def on_truck_end_stationary(self, truck):
        if not self.is_loaded:
            self.world.hint_text = self.please_stop_msg
            if self.trash_stream is not None:
                self.world.destroy_actor(self.trash_stream)

    def tick(self, dt):
        if self.can_load and self.is_truck_stationary and not self.is_loaded:
            # Increment player fullness.
            self.cargo_full_percent -= dt * 0.001 * dock_speed

            if self.cargo_full_percent <= 0.0:
                # Reached full cargo offload.
                self.cargo_full_percent = 0.0
                self.world.hint_text = self.please_leave_area_msg
                self.is_loaded = True
                self.should_visualise_area = False
                if self.trash_stream is not None:
                    self.world.destroy_actor(self.trash_stream)

                player_position = self.world.player.location
                self.world.destroy_actor(self.world.player)

                loaded_truck_class = GameplayStatics.persistent_world_cache["vehicle_class_side"]
                GameplayStatics.persistent_world_cache["mission_has_dumped"] = True
                self.world.player = self.world.spawn_actor(
                    loaded_truck_class, player_position)

            else:
                # Still filling
                self.world.hint_text = "Offloading truck %.2f%%. Please wait" % (
                    self.cargo_full_percent * 100)

        super().tick(dt)


class WorldTravelDock(TruckDockBase):
    """Point which the player can enter to travel to another world.

    Make sure you set:
        name: Name of the world this travels to (used for UI and gameplay statics persistent data caching)
        connecting_world_class: World this travels to. If omitted, attempts to use gameplay statics persistent data caching

    These are public attributes and control the behaviour
    of world traveling.
    """

    def __init__(self):
        super().__init__()

        self.name = "DefaultArea"
        self.connecting_world_class = None

    def begin_play(self):
        super().begin_play()

        GameplayStatics.game_engine.input_mappings.bind_action(
            "ConfirmAction", EInputEvent.PRESSED, self.on_confirm_action)

    def on_confirm_action(self):
        connecting_class = self.connecting_world_class
        if connecting_class is None:
            # Try get world class from name
            connecting_class = GameplayStatics.persistent_world_cache["key_area_classes"].get(self.name)

        if self.is_truck_stationary and connecting_class is not None:
            #GameplayStatics.persistent_world_cache["last_key_area"] = self.name
            GameplayUtilities.travel(connecting_class)

    def on_begin_cursor_over(self, event):
        if self.world.hint_text.startswith("Remain") or self.world.hint_text.startswith("Press"):
            return
        if not self.is_truck_inside:
            self.world.hint_text = "Cursor: %s (Key Area)" % self.name

    def on_end_cursor_over(self, event):
        if not self.is_truck_inside:
            self.world.hint_text = ""

    def on_truck_enter(self, truck):
        self.world.hint_text = "Remain stationary to travel to %s" % self.name

    def on_truck_leave(self, truck):
        pass

    def on_truck_begin_stationary(self, truck):
        self.world.hint_text = "Press F to travel to %s" % self.name

    def on_truck_end_stationary(self, truck):
        pass
