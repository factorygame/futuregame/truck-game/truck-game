"""Helper classes for building up a world of vector graphics.
"""

from factorygame.core.engine_base import ETickGroup, EActorMovementMode
from factorygame.core.blueprint import PolygonNode


class SceneryBase(PolygonNode):
    """Base class for uninteractive world scenery.
    """

    def __init__(self):
        super().__init__()
        self.movement_mode = EActorMovementMode.STATIC
        self.generate_click_events = False
        self.generate_cursor_over_events = False


class ForegroundScenery(SceneryBase):
    """Base class for world scenery displayed in UI tick group.
    """

    def __init__(self):
        super().__init__()

        self.primary_actor_tick.tick_group = ETickGroup.UI


class BackgroundScenery(SceneryBase):
    """Base class for world scenery displayed in world tick group.
    """

    def __init__(self):
        super().__init__()

        self.primary_actor_tick.tick_group = ETickGroup.WORLD
