from factorygame import Loc, GameplayStatics, FColor
from factorygame.core.blueprint import WorldGraph
from truckgame.core.world_building import ForegroundScenery, BackgroundScenery
from truckgame.core.waste_chute import WasteChuteBase, TruckLoadingDock, WorldTravelDock, TruckOffloadingDock
from truckgame.maps.world_view import TruckWorldViewWorld
from truckgame.menus.hud import KeyAreaHud


class IndustrialFactoryWorld(WorldGraph):
    """Key area for side view Industrial Factory.
    """

    def __init__(self):
        super().__init__()

        # Optimise rendering if possible
        self.enable_batch_render_delete = True

        self.allow_world_drag_navigation = False

        self.hint_text = ""

        self.area_name = "John's Toy Factory"

        self.player_start = Loc(2000, 0)
        self.world_dock_start = Loc(2600, 0)

    def is_mission_area(self):
        """Return whether this world is the correct mission area.
        """
        mission_area = GameplayStatics.persistent_world_cache.get("mission_area")
        return mission_area and mission_area == self.area_name

    def begin_play(self):
        super().begin_play()

        GameplayStatics.persistent_world_cache["mission_area"] = self.area_name
        GameplayStatics.persistent_world_cache["last_key_area"] = self.area_name

        # Create the scene
        self.create_scenery()

        # Spawn player
        player_class = GameplayStatics.persistent_world_cache["vehicle_class_side"]
        self.player = self.spawn_actor(player_class, self.player_start)

        # Make sure player can get back to the world!
        world_dock = self.spawn_actor(WorldTravelDock, self.world_dock_start)
        world_dock.name = "World View"
        #world_dock.connecting_world_class = GameplayStatics.persistent_world_cache["key_area_classes"]["World View"]

        #GameplayStatics.persistent_world_cache["mission_key_area"] = None

        self.hud = self.spawn_actor(KeyAreaHud, Loc(0, 0))

        self.zoom_ratio = 15

    def create_scenery(self):

        ground = self.spawn_actor(BackgroundScenery, Loc(0, 0))
        ground.vertices = [Loc(-5399.999999999991, 0.3673469387264845), Loc(-600.2448979591408, -1.4693877551536048), Loc(-478.6530612244479, -1.1020408163774533), Loc(-316.653061224448, 6.979591836683767), Loc(-130.04081632649155, -3.6734693878055538), Loc(121.95918367350617, -1.4693877551523542), Loc(301.95918367350635, 1.4693877550516277), Loc(623.7551020408532, -3.6734693878053406), Loc(6300.734693877592, 0.3673469387258592), Loc(6300.367346938835, -2097.1836734694466), Loc(-5400.36734693875, -1801.4693877551708)]
        ground.fill_color = FColor(214, 202, 176)
        ground.drawable_padding = Loc(10000, 10000)

        factory = self.spawn_actor(ForegroundScenery, Loc(-600, -50))
        factory.vertices = [Loc(299.02040816326576, -1.469387755108528), Loc(305.6326530612248, 897.7959183673404), Loc(193.224489795919, 785.3877551020341), Loc(206.44897959183663, 1497.306122448969), Loc(87.42857142857133, 1501.7142857142755), Loc(80.81632653061206, 695.0204081632551), Loc(-0.7346938775510807, 598.0408163265204), Loc(-2.938775510204323, 897.7959183673368), Loc(-304.8979591836728, 600.244897959175), Loc(-307.102040816324, 899.9999999999909), Loc(-397.46938775509966, 818.4489795918277), Loc(-373.2244897959158, 1360.6530612244783), Loc(-474.61224489795677, 1362.8571428571313), Loc(-490.0408163265281, 739.1020408163172), Loc(-604.6530612244871, 602.4489795918275), Loc(-600.2448979591811, 906.61224489795), Loc(-904.4081632653017, 626.6938775510116), Loc(-897.7959183673424, 897.795918367338), Loc(-1206.3673469387709, 604.653061224481), Loc(-1200.4897959183575, 0.734693877537552)]
        factory.fill_color = FColor(235, 80, 56)

        # THESE TWO ARE THE IMPORTANT ONES!
        self.waste_chute = self.spawn_actor(WasteChuteBase, Loc(-300, 300))
        self.dock = self.spawn_actor(TruckLoadingDock, Loc(-130, -30))


# I'm just going to define all the worlds in here for convenience


class IndustrialDumpWorld(IndustrialFactoryWorld):
    """Key area for side view Industrial Dump.
    """

    def __init__(self):
        super().__init__()

        self.area_name = "Industrial Dump"

        self.player_start = Loc(-1200, 0)
        self.world_dock_start = Loc(-2000, 0)

    def begin_play(self):
        super().begin_play()
        # Swap player vertices x axis.
        self.player.vertices = [vert * (-1, 1) for vert in self.player.vertices]

    def create_scenery(self):
        ground = self.spawn_actor(BackgroundScenery, Loc(0, 0))
        ground.vertices = [Loc(-5399.999999999991, 0.3673469387264845), Loc(-600.2448979591408, -1.4693877551536048), Loc(-478.6530612244479, -1.1020408163774533), Loc(-316.653061224448, 6.979591836683767), Loc(-130.04081632649155, -3.6734693878055538), Loc(121.95918367350617, -1.4693877551523542), Loc(301.95918367350635, 1.4693877550516277), Loc(623.7551020408532, -3.6734693878053406), Loc(6300.734693877592, 0.3673469387258592), Loc(6300.367346938835, -2097.1836734694466), Loc(-5400.36734693875, -1801.4693877551708)]
        ground.fill_color = FColor(200)

        truck_sucker = self.spawn_actor(ForegroundScenery, Loc(300, 0))
        truck_sucker.vertices = [Loc(-49.59183673468806, -0.3673469387898649), Loc(-58.40816326530023, 53.99999999998562), Loc(-115.71428571427987, 55.46938775508761), Loc(-111.30612244897372, 130.4081632652917), Loc(-48.12244897958601, 124.53061224488363), Loc(-31.959183673463485, 368.4489795918223), Loc(98.81632653061808, 375.79591836733243), Loc(90.0000000000058, -9.183673469402152)]
        truck_sucker.fill_color = FColor.red()

        hole = self.spawn_actor(ForegroundScenery, Loc(1500, 0))
        hole.vertices = [Loc(-1084.040816326523, 15.795918367339482), Loc(-1086.9795918367272, -107.63265306123185), Loc(-998.8163265306048, -815.8775510204155), Loc(896.6938775510278, -898.1632653061297), Loc(1128.8571428571502, -107.63265306123185), Loc(1155.3061224489868, 42.244897959176114)]
        hole.fill_color = FColor(150)
        hole.drawable_padding = Loc(5000, 5000)


        self.waste_chute = self.spawn_actor(WasteChuteBase, Loc(400, 100))
        self.dock = self.spawn_actor(TruckOffloadingDock, Loc(130, -30))


class CityConstructionSiteWorld(IndustrialFactoryWorld):
    """Key area for side view City Construction Site.
    """

    def __init__(self):
        super().__init__()

        self.area_name = "City Construction Site"

        #self.player_start = Loc(-600, 0)
        #self.world_dock_start = Loc(-1200, 0)

    def begin_play(self):
        super().begin_play()
        # Swap player vertices x axis.
        #self.player.vertices = [vert * (-1, 1) for vert in self.player.vertices]

    def create_scenery(self):

        ground = self.spawn_actor(BackgroundScenery, Loc(0, 0))
        ground.vertices = [Loc(-5399.999999999991, 0.3673469387264845), Loc(-600.2448979591408, -1.4693877551536048), Loc(-478.6530612244479, -1.1020408163774533), Loc(-316.653061224448, 6.979591836683767), Loc(-130.04081632649155, -3.6734693878055538), Loc(121.95918367350617, -1.4693877551523542), Loc(301.95918367350635, 1.4693877550516277), Loc(623.7551020408532, -3.6734693878053406), Loc(6300.734693877592, 0.3673469387258592), Loc(6300.367346938835, -2097.1836734694466), Loc(-5400.36734693875, -1801.4693877551708)]
        ground.fill_color = FColor(214, 202, 176)
        ground.drawable_padding = Loc(10000, 10000)

        factory = self.spawn_actor(ForegroundScenery, Loc(-600, -50))
        factory.vertices = [Loc(301.9591836734787, -1.4693877551216303), Loc(296.8163265306216, 906.2448979591642), Loc(-304.89795918366417, 908.8163265305926), Loc(-304.89795918366417, 3.67346938773548), Loc(-577.4693877550926, 1.1020408163069249), Loc(-564.6122448979498, 584.8163265305927), Loc(-883.4693877550926, 613.102040816307), Loc(-906.6122448979497, 8.816326530592619), Loc(-1045.4693877550926, 13.959183673449786), Loc(-999.1836734693784, 1657.1020408163004), Loc(-1127.7551020408073, 1698.244897959155), Loc(-1292.3265306122357, 1690.5306122448692), Loc(-1359.1836734693786, 1641.6734693877265), Loc(-1395.1836734693807, 3.6734693877159543), Loc(-1392.6122448979522, -68.32653061228564), Loc(291.67346938776245, -70.8979591837176)]
        factory.fill_color = FColor(206, 219, 219)

        crane = self.spawn_actor(ForegroundScenery, Loc(-600, -50))
        crane.vertices = [Loc(-78.61224489794995, 903.6734693876875), Loc(116.81632653062138, 893.3877551019733), Loc(139.95918367347963, 1315.1020408162467), Loc(906.2448979591938, 1338.244897959104), Loc(933.7959183673573, 1324.2857142856333), Loc(934.8979591836838, 1112.6938775509393), Loc(869.8775510204187, 1115.999999999919), Loc(867.6734693877655, 1082.938775510123), Loc(1053.9183673469493, 1082.938775510123), Loc(1057.2244897959288, 1120.408163265225), Loc(975.6734693877655, 1115.999999999919), Loc(969.0612244898064, 1313.2653061223677), Loc(1005.4285714285819, 1346.3265306121639), Loc(1031.8775510204187, 1373.877551020327), Loc(1005.4285714285819, 1391.5102040815514), Loc(139.224489795929, 1384.897959183593), Loc(57.67346938776586, 1470.8571428570622), Loc(-31.591836734683056, 1421.2653061223682), Loc(-52.53061224488715, 1372.775510204001), Loc(-514.2857142857006, 1403.632653061144), Loc(-503.26530612243533, 1309.959183673389), Loc(-56.93877551019057, 1320.9795918366544)]
        crane.fill_color = FColor(237, 229, 114)

        # THESE TWO ARE THE IMPORTANT ONES!
        self.waste_chute = self.spawn_actor(WasteChuteBase, Loc(-300, 300))
        self.dock = self.spawn_actor(TruckLoadingDock, Loc(-130, -30))


class NuclearPowerPlantWorld(IndustrialFactoryWorld):
    """Key area for side view Nuclear Power Plant.
    """

    def __init__(self):
        super().__init__()

        self.area_name = "Nuclear Power Plant"

        #self.player_start = Loc(-600, 0)
        #self.world_dock_start = Loc(-1200, 0)

    def begin_play(self):
        super().begin_play()
        # Swap player vertices x axis.
        #self.player.vertices = [vert * (-1, 1) for vert in self.player.vertices]

    def create_scenery(self):

        ground = self.spawn_actor(BackgroundScenery, Loc(0, 0))
        ground.vertices = [Loc(-5399.999999999991, 0.3673469387264845), Loc(-600.2448979591408, -1.4693877551536048), Loc(-478.6530612244479, -1.1020408163774533), Loc(-316.653061224448, 6.979591836683767), Loc(-130.04081632649155, -3.6734693878055538), Loc(121.95918367350617, -1.4693877551523542), Loc(301.95918367350635, 1.4693877550516277), Loc(623.7551020408532, -3.6734693878053406), Loc(6300.734693877592, 0.3673469387258592), Loc(6300.367346938835, -2097.1836734694466), Loc(-5400.36734693875, -1801.4693877551708)]
        ground.fill_color = FColor(214, 202, 176)
        ground.drawable_padding = Loc(10000, 10000)

        silos = self.spawn_actor(ForegroundScenery, Loc(-500,0))
        silos.vertices = [Loc(214.89795918369236, -1.4693877551661103), Loc(207.18367346940636, 661.9591836734054), Loc(112.0408163265497, 751.9591836734052), Loc(19.469387755120806, 777.673469387691), Loc(-199.10204081630764, 808.5306122448338), Loc(-307.10204081630764, 772.5306122448338), Loc(-420.24489795916475, 726.2448979591196), Loc(-476.8163265305934, 695.3877551019765), Loc(-497.38775510202186, 3.6734693876911138)]
        silos.fill_color = FColor(196, 209, 167)

        cooler = self.spawn_actor(ForegroundScenery, Loc(-1500, -30))
        cooler.vertices = [Loc(472.04081632654857, 6.2448979591219995), Loc(346.04081632654857, 191.3877551019791), Loc(271.46938775511967, 435.67346938769333), Loc(266.3265306122628, 682.5306122448361), Loc(274.04081632654857, 813.6734693876934), Loc(281.7551020408339, 978.2448979591219), Loc(268.8979591836912, 1091.3877551019787), Loc(99.18367346940568, 1057.9591836734076), Loc(-116.81632653059455, 1099.102040816265), Loc(-186.24489795916588, 1096.530612244836), Loc(-175.95918367345143, 872.8163265305506), Loc(-160.53061224488033, 564.2448979591219), Loc(-191.38775510202322, 412.5306122448362), Loc(-268.5306122448801, 281.3877551019791), Loc(-353.387755102023, 127.10204081626489), Loc(-433.10204081630854, 39.673469387693444)]
        cooler.fill_color = FColor(161, 161, 161)

        cooler = self.spawn_actor(ForegroundScenery, Loc(-2500, -30))
        cooler.vertices = [Loc(472.04081632654857, 6.2448979591219995), Loc(346.04081632654857, 191.3877551019791), Loc(271.46938775511967, 435.67346938769333), Loc(266.3265306122628, 682.5306122448361), Loc(274.04081632654857, 813.6734693876934), Loc(281.7551020408339, 978.2448979591219), Loc(268.8979591836912, 1091.3877551019787), Loc(99.18367346940568, 1057.9591836734076), Loc(-116.81632653059455, 1099.102040816265), Loc(-186.24489795916588, 1096.530612244836), Loc(-175.95918367345143, 872.8163265305506), Loc(-160.53061224488033, 564.2448979591219), Loc(-191.38775510202322, 412.5306122448362), Loc(-268.5306122448801, 281.3877551019791), Loc(-353.387755102023, 127.10204081626489), Loc(-433.10204081630854, 39.673469387693444)]
        cooler.fill_color = FColor(161, 161, 161)

        # THESE TWO ARE THE IMPORTANT ONES!
        self.waste_chute = self.spawn_actor(WasteChuteBase, Loc(-300, 300))
        self.dock = self.spawn_actor(TruckLoadingDock, Loc(-130, -30))


class NuclearTreatmentFacilityWorld(IndustrialFactoryWorld):
    """Key area for side view Nuclear Treatment Facility.
    """

    def __init__(self):
        super().__init__()

        self.area_name = "Nuclear Treatment Facility"

        self.player_start = Loc(-1200, 0)
        self.world_dock_start = Loc(-2000, 0)

    def begin_play(self):
        super().begin_play()
        # Swap player vertices x axis.
        self.player.vertices = [vert * (-1, 1) for vert in self.player.vertices]

    def create_scenery(self):
        ground = self.spawn_actor(BackgroundScenery, Loc(0, 0))
        ground.vertices = [Loc(-5399.999999999991, 0.3673469387264845), Loc(-600.2448979591408, -1.4693877551536048), Loc(-478.6530612244479, -1.1020408163774533), Loc(-316.653061224448, 6.979591836683767), Loc(-130.04081632649155, -3.6734693878055538), Loc(121.95918367350617, -1.4693877551523542), Loc(301.95918367350635, 1.4693877550516277), Loc(623.7551020408532, -3.6734693878053406), Loc(6300.734693877592, 0.3673469387258592), Loc(6300.367346938835, -2097.1836734694466), Loc(-5400.36734693875, -1801.4693877551708)]
        ground.fill_color = FColor(200)

        truck_sucker = self.spawn_actor(ForegroundScenery, Loc(300, 0))
        truck_sucker.vertices = [Loc(-49.59183673468806, -0.3673469387898649), Loc(-58.40816326530023, 53.99999999998562), Loc(-115.71428571427987, 55.46938775508761), Loc(-111.30612244897372, 130.4081632652917), Loc(-48.12244897958601, 124.53061224488363), Loc(-31.959183673463485, 368.4489795918223), Loc(98.81632653061808, 375.79591836733243), Loc(90.0000000000058, -9.183673469402152)]
        truck_sucker.fill_color = FColor(181, 196, 59)

        hole = self.spawn_actor(ForegroundScenery, Loc(1500, 0))
        hole.vertices = [Loc(-1084.040816326523, 15.795918367339482), Loc(-1086.9795918367272, -107.63265306123185), Loc(-998.8163265306048, -815.8775510204155), Loc(896.6938775510278, -898.1632653061297), Loc(1128.8571428571502, -107.63265306123185), Loc(1155.3061224489868, 42.244897959176114)]
        hole.fill_color = FColor(88, 92, 56)
        hole.drawable_padding = Loc(5000, 5000)


        self.waste_chute = self.spawn_actor(WasteChuteBase, Loc(400, 100))
        self.dock = self.spawn_actor(TruckOffloadingDock, Loc(130, -30))
