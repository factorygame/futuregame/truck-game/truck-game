from tkinter.ttk import Button
from factorygame import Loc, GameplayUtilities, GameplayStatics, FColor
from factorygame.core.blueprint import WorldGraph
from truckgame.maps.side_drive_test import TruckSideDriveTestWorld
from truckgame.maps.industrial_factory import (IndustrialFactoryWorld,
    IndustrialDumpWorld, CityConstructionSiteWorld, NuclearPowerPlantWorld,
    NuclearTreatmentFacilityWorld)
from truckgame.maps.world_view import TruckWorldViewWorld
from truckgame.maps.office import OfficeWorld
from truckgame.vehicles.trucks import (
    SmallGreyTruckSide, SmallGreyTruckTop, SmallGreyTruckSideLoaded,
    LongRedTruckSide, LongRedTruckTop)

class TruckMainMenuWorld(WorldGraph):
    """Top down world view to travel between key areas.
    """

    def begin_play(self):
        super().begin_play()
        self.create_text(Loc(20, 20),
            text="Garbage Truck Simulator 2019",
            font=("Arial", 24),
            anchor="nw")

        self.create_text(Loc(40, 60),
            text="Welcome to Garbage Truck Simulator 2019",
            anchor="nw")

        self.create_text(Loc(40, 100),
            text="Credits",
            anchor="nw")

        self.create_text(Loc(60, 120),
            text="Creator: David Kanekanian",
            anchor="nw")

        self.create_text(Loc(60, 140),
            text="Voice Acting: MonologueMan (from Twitch!)",
            anchor="nw")

        my_btn = Button(self, text="Start",
            command=lambda: GameplayUtilities.travel(OfficeWorld))
        self.create_window(
            Loc(40, 180),
            window=my_btn,
            anchor="nw")

        vertices = [Loc(262.28571428571627, 3.6734693877580753), Loc(287.6326530612266, 98.4489795918397), Loc(190.6530612244918, 116.08163265306416), Loc(131.14285714285916, 280.2857142857173), Loc(48.489795918369396, 304.5306122449009), Loc(-20.938775510201936, 299.02040816326826), Loc(-79.34693877550819, 300.1224489795949), Loc(-73.83673469387554, 146.938775510207), Loc(-89.26530612244687, 54.367346938778496), Loc(-159.79591836734494, 49.95918367347235), Loc(-161.99999999999795, 342.00000000000296), Loc(-391.2244897959125, 344.2040816326563), Loc(-570.8571428571371, 325.46938775510534), Loc(-807.7959183673411, 349.71428571428896), Loc(-860.6938775510146, 308.9387755102074), Loc(-862.8979591836676, 57.673469387758416), Loc(-843.06122448979, 8.08163265306456), Loc(-719.6326530612193, 3.673469387758473), Loc(-720.7346938775457, -19.469387755098694), Loc(-654.6122448979539, -16.163265306119115), Loc(-661.2244897959131, 8.08163265306456), Loc(-386.81632653060706, 3.673469387758473), Loc(-369.9183673469339, -27.183673469383734), Loc(-318.48979591836246, -16.163265306118433), Loc(-295.7142857142809, 16.89795918367747), Loc(-40.77551020407873, -3.6734693877516236), Loc(80.4489795918393, -1.4693877550985235), Loc(83.38775510204329, -24.24489795918015), Loc(140.69387755102304, -8.816326530608734), Loc(167.14285714285967, 8.816326530615783)]
        self.create_polygon(
            *[v * Loc(0.2, -0.2) + Loc(200, 290) for v in vertices],
            fill=FColor(230, 108, 108).to_hex(),
            outline=FColor(230, 108, 108).to_hex())

        GameplayStatics.root_window.geometry("1280x720")

        # set test data

        GameplayStatics.persistent_world_cache["first_time_startup"] = True
        GameplayStatics.persistent_world_cache["vehicle_class_side"] = LongRedTruckSide
        GameplayStatics.persistent_world_cache["vehicle_class_top"] = LongRedTruckTop
        GameplayStatics.persistent_world_cache["vehicle_class_side_loaded"] = LongRedTruckSide

        GameplayStatics.persistent_world_cache["last_key_area"] = None

        # Starting values are preloaded with John's Toy Factory contract values
        GameplayStatics.persistent_world_cache["mission_name"] = "John's Toy Factory"
        GameplayStatics.persistent_world_cache["mission_load_area"] = "John's Toy Factory"
        GameplayStatics.persistent_world_cache["mission_dump_area"] = "Industrial Dump"
        GameplayStatics.persistent_world_cache["mission_has_loaded"] = False
        GameplayStatics.persistent_world_cache["mission_has_dumped"] = False

        GameplayStatics.persistent_world_cache["key_area_classes"] = {
            "World View": TruckWorldViewWorld,
            "John's Toy Factory": IndustrialFactoryWorld,
            "Industrial Dump": IndustrialDumpWorld,
            "City Construction Site": CityConstructionSiteWorld,
            "Nuclear Power Plant": NuclearPowerPlantWorld,
            "Nuclear Treatment Facility": NuclearTreatmentFacilityWorld,

            "DefaultArea": IndustrialFactoryWorld,
            "Office": OfficeWorld
            }

        GameplayStatics.persistent_world_cache["available_contracts"] = [
            "John's Toy Factory", "Construction Site", "Nuclear Treatment"]#, "Construction Site", "Nuclear Treatment"]

        #GameplayUtilities.travel(TruckSideDriveTestWorld)
        #GameplayUtilities.travel(IndustrialFactoryWorld)
        #GameplayUtilities.travel(TruckWorldViewWorld)
        #GameplayUtilities.travel(IndustrialDumpWorld)
        #GameplayUtilities.travel(OfficeWorld)
