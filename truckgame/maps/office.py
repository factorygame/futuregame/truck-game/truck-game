"""Office to get a new garbage contract.
"""

from tkinter import *
from tkinter.ttk import *

import winsound

from factorygame import GameplayStatics, FColor, Loc, Actor, GameplayUtilities
from factorygame.core.blueprint import WorldGraph, PolygonNode


class MyPolygonNode(PolygonNode):
    def __init__(self):
        super().__init__()
        self.primary_actor_tick.tick_group = 4


class TkContractMenu(Actor):
    """Use tkinter for real UI contract choosing experience.
    """

    def begin_play(self):

        self.root = Toplevel(GameplayStatics.root_window)
        self.root.protocol("WM_DELETE_WINDOW", self.on_window_destroy)
        self.root.title("Contract Chooser")
        #self.root.pack()

        top_frame = Frame(self.root)
        top_frame.pack(fill="both")

        # Contracts

        contracts = LabelFrame(top_frame, text="Contracts")
        contracts.pack(side="left", fill="both")

        self.contract_list = Listbox(contracts)
        self.contract_list.pack()

        for item in GameplayStatics.persistent_world_cache["available_contracts"]:
            self.contract_list.insert("end", item)

        # Details

        details = LabelFrame(top_frame, text="Details")
        details.pack(side="left", fill="both")

        self.details_var = StringVar()
        self.details_var.set("")
        Label(details, textvariable=self.details_var, width=50).pack()

        # Accept
        self.accept_button = Button(
            self.root, text="Accept", command=self.on_accept_contract,
            state="disabled")
        self.accept_button.pack(padx=10, pady=10)


        self.last_selected = -1

        self.mission_details = {
        "John's Toy Factory":
            "Hello pal, I’m the manager of John's Toy Factory!\n"
            "We manufacture children’s toys of all kinds! Part\n"
            "of our job involves painting the cute little patterns\n"
            "on the toys. Our talented painters at the factory go\n"
            "through TONS of paint every day, and we need you to\n"
            "come collect all our empty paint cans and send them\n"
            "to the industrial waste facility. Are you up for it?",

        "Construction Site":
            "Hmm, you’re new around here, aren’t you? I’m Sid by\n"
            "the way, construction work manager in the city.\n"
            "We’re putting up some fancy new offices, but the\n"
            "work’s been ever so slow lately. The garbage here is\n"
            "piling up, you see. Ever since that freak rain storm\n"
            "began, the garbage collection agencies have been\n"
            "telling me its too wet or too windy to collect it!\n"
            "What we need is someone who’s not afraid to get their\n"
            "hands dirty and take our rubbish away so we can back\n"
            "get to work!",

        "Nuclear Treatment":
            "So you’re the new garbage truck driver in town?\n"
            "I’ve heard all about you, thought I’d see for myself\n"
            "just how good you really are. We’ve got a big nuclear\n"
            "power site, feeding the whole city, and the power demand\n"
            "increased in the past few months. That leaves us with a\n"
            "load of deadly waste on our hands. If your think you’re up\n"
            "for it, you won’t have a problem disposing of our nuclear\n"
            "waste, I suppose?"}

    def get_selected_mission_name(self):
        if not self.contract_list.winfo_exists():
            return

        curselection = self.contract_list.curselection()
        if len(curselection) == 0:
            return

        selected_index, *_ = curselection

        if not isinstance(selected_index, str):
            mission_name = GameplayStatics.persistent_world_cache["available_contracts"][selected_index]
        else:
            mission_name = selected_index

        return mission_name

    def tick(self, dt):
        mission_name = self.get_selected_mission_name()
        if not mission_name:
            return

        if mission_name is not None and mission_name != self.last_selected:
            self.last_selected = mission_name
        # Selection has changed at this point.


            # Enable button for confirming input
            self.accept_button.config(state="normal")

            # Show text
            self.details_var.set(self.mission_details[mission_name])

            # Play dialogue
            filename = "truckgame/dialogue/%s_contract_start.wav" % mission_name
            winsound.PlaySound(filename,
                winsound.SND_FILENAME | winsound.SND_ASYNC)

    def on_accept_contract(self):
        mission_name = self.get_selected_mission_name()
        if not mission_name:
            return

        if mission_name == "John's Toy Factory":
            GameplayStatics.persistent_world_cache["mission_load_area"] = "John's Toy Factory"
            GameplayStatics.persistent_world_cache["mission_dump_area"] = "Industrial Dump"

        elif mission_name == "Construction Site":
            GameplayStatics.persistent_world_cache["mission_load_area"] = "City Construction Site"
            GameplayStatics.persistent_world_cache["mission_dump_area"] = "Industrial Dump"

        elif mission_name == "Nuclear Treatment":
            GameplayStatics.persistent_world_cache["mission_load_area"] = "Nuclear Power Plant"
            GameplayStatics.persistent_world_cache["mission_dump_area"] = "Nuclear Treatment Facility"

        GameplayStatics.persistent_world_cache["last_key_area"] = "Office"
        GameplayStatics.persistent_world_cache["mission_name"] = mission_name
        GameplayStatics.persistent_world_cache["mission_has_loaded"] = False
        GameplayStatics.persistent_world_cache["mission_has_dumped"] = False

        self.on_window_destroy()
        GameplayUtilities.travel(GameplayStatics.persistent_world_cache["key_area_classes"]["World View"])

    def on_window_destroy(self):
        if self.root.winfo_exists():
            self.root.destroy()
        winsound.PlaySound(None, winsound.SND_PURGE)

    def begin_destroy(self):
        if self.root.winfo_exists():
            self.root.destroy()
        super().begin_destroy()


class OfficeWorld(WorldGraph):
    """Key area for office.
    """

    def begin_play(self):
        super().begin_play()

        # Create computer monitor
        computer = self.spawn_actor(PolygonNode, Loc(0, -600))
        computer.vertices = [Loc(-395.2653061224496, 67.22448979591763), Loc(-390.85714285714334, -14.326530612245563), Loc(402.61224489795836, -14.326530612245563), Loc(411.4285714285709, 91.46938775510137), Loc(49.959183673468715, 98.08163265306052), Loc(56.57142857142799, 203.8775510204074), Loc(592.1632653061217, 212.69387755101968), Loc(605.38775510204, 984.122448979591), Loc(-514.2857142857149, 964.2857142857134), Loc(-518.693877551021, 210.48979591836667), Loc(-58.04081632653106, 221.51020408163185), Loc(-62.448979591837315, 95.8775510204074)]
        computer.fill_color = FColor(180)
        computer.on_click = lambda e: self.create_contract_menu()

        cursor = self.spawn_actor(MyPolygonNode, Loc(200, -500))
        cursor.vertices = [Loc(-183.67346938775495, 649.1020408163191), Loc(-183.67346938775495, 391.2244897959109), Loc(-84.48979591836701, 391.2244897959109), Loc(-38.20408163265279, 283.2244897959109), Loc(-7.346938775509898, 283.2244897959109), Loc(-44.81632653061183, 413.26530612244153), Loc(49.959183673469624, 406.65306122448237)]
        cursor.fill_color = FColor(250)

        self.zoom_ratio = 6

        self.menu = None

        if GameplayStatics.persistent_world_cache["first_time_startup"]:
            # Play welcome dialogue
            GameplayStatics.persistent_world_cache["first_time_startup"] = False
            filename = "truckgame/dialogue/welcome.wav"
            winsound.PlaySound(filename,
                winsound.SND_FILENAME | winsound.SND_ASYNC)

    def create_contract_menu(self):
        if self.menu:
            self.destroy_actor(self.menu)

        self.menu = self.spawn_actor(TkContractMenu, Loc(0, 0))
