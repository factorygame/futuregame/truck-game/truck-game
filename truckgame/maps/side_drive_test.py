from factorygame import Loc
from factorygame.core.blueprint import WorldGraph
from truckgame.vehicles.trucks import SmallGreyTruckTop

class TruckSideDriveTestWorld(WorldGraph):
    """Side view to drive truck.
    """

    def begin_play(self):
        super().begin_play()
        self.truck = self.spawn_actor(SmallGreyTruckTop, Loc(0, 0))
