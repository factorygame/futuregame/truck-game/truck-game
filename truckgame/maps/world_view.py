from factorygame import Loc, GameplayStatics, FColor
from factorygame.core.blueprint import (WorldGraph, GridGismo, PolygonNode,
    GeomHelper)
from truckgame.core.world_building import ForegroundScenery, BackgroundScenery
from truckgame.core.waste_chute import WorldTravelDock
#from truckgame.maps.industrial_factory import IndustrialFactoryWorld
from truckgame.menus.hud import WorldViewHud


class MapTheme:
    """Color constants for modern online map service theme.
    """

    BACKGROUND = FColor(237, 236, 233)
    WATER = FColor(170, 218, 255)
    PARK = FColor(200, 234, 200)
    INDUSTRY = FColor(207, 143, 6)
    ROAD = FColor(255, 242, 175)
    TEXT = FColor(104, 119, 135)
    CITY = FColor(201)


class MapViewRoad(BackgroundScenery):
    """Line to follow to get to places.

    Inherits PolygonNode for easier customisation, but draws line.
    """

    def __init__(self):
        super().__init__()
        self.fill_color = MapTheme.ROAD
        self.outline_width = 5.0

        self.generate_cursor_over_events = True

    def _should_draw(self):
        """Always draw roads.
        """
        return True

    def _draw(self):
        # Create a generator to convert vertices into canvas coordinates.
        transpose_func = self.world.view_to_canvas
        transposed_verts = map(transpose_func, self.world_vertices)

        new_id = self.world.create_line(
            *transposed_verts,
            fill=self._fill_color_hex,
            width=self.outline_width,
            tags=(self.unique_id))

        self.register_canvas_id(new_id)


class MapViewArea(BackgroundScenery):
    """Major area on map designated by color.

    Shows area name in hint field when cursor over.
    """

    def __init__(self):
        super().__init__()
        self.generate_cursor_over_events = True
        self.area_type = "park"

    def on_begin_cursor_over(self, event):
        if self.world.hint_text.startswith("Remain") or self.world.hint_text.startswith("Press"):
            return

        self.world.hint_text = "Cursor: %s" % self.area_type.title()

    def on_end_cursor_over(self, event):
        if self.world.hint_text.startswith("Remain") or self.world.hint_text.startswith("Press"):
            return

        self.world.hint_text = ""


class MapKeyArea(WorldTravelDock):
    """Point which the player can enter to travel to side view.

    Each key area designates only ONE side view world.
    """

    def begin_play(self):
        super().begin_play()
        self.vertices = tuple(GeomHelper.generate_reg_poly(6, radius=120))
        self.length = 200.0
        self.height = 200.0


class TruckWorldViewWorld(WorldGraph):
    """Top down world view to travel between key areas.
    """

    def __init__(self):
        super().__init__()

        # Optimise rendering if possible
        self.enable_batch_render_delete = False

        self.allow_world_drag_navigation = False

        self.hint_text = ""

        # We're really overdue a confusing PlayerStart actor system!
        self._player_start_location = Loc(0, 0)

        # Google maps style theme
        self.config(bg=MapTheme.BACKGROUND.to_hex())

    def begin_play(self):
        super().begin_play()

        # Create the scene
        self.create_scenery()

        # Spawn player at last location
        player_class = GameplayStatics.persistent_world_cache["vehicle_class_top"]
        self.player = self.spawn_actor(player_class, self._player_start_location + 120)

        self.hud = self.spawn_actor(WorldViewHud, Loc(0, 0))

        self.zoom_ratio = 9

    def create_scenery(self):

##        grid = self.spawn_actor(GridGismo, Loc(0, 0))
##        grid.grid_line_color = MapTheme.TEXT
##        grid.grid_text_color = MapTheme.TEXT
##        grid.origin_line_color = MapTheme.TEXT

        self.__draw_map_areas()
        self.__draw_roads()
        self.__draw_key_areas()

    def __draw_map_areas(self):
        """Draw map areas (colored areas). Not KeyAreas!
        """

        areas = (
            {"location": Loc(300, 300),
             "vertices": [Loc(-394.53061224489556, -80.08163265306882), Loc(-258.24489795918134, -532.6530612244974), Loc(418.0408163265329, -522.367346938783), Loc(500.3265306122471, 362.2040816326455), Loc(-224.81632653061, 467.63265306121684)],
             "fill_color": MapTheme.WATER,
             "area_type": "lake"
            },
            {"location": Loc(2000, -400),
             "vertices": [Loc(-325.40573094955516, 864.4661128163791), Loc(-256.38823051010195, 303.32382663473845), Loc(-388.4217096116646, -263.8199813242459), Loc(-571.4681238206489, -707.9325928476834), Loc(-49.33572919174276, -890.9790070566678), Loc(616.8331880934136, -905.9828115000271), Loc(622.8347098707575, -452.86791731057406), Loc(685.850688532867, -8.755305787136649), Loc(919.9100378492731, 465.3649146230198), Loc(655.843079646148, 981.4957874745821), Loc(181.72285923599156, 1119.5307883534886), Loc(97.70155435317929, 780.4448079335664)],
             "fill_color": MapTheme.PARK,
             "area_type": "local park"
            },
            {"location": Loc(0, 4000),
             "vertices": [Loc(-394.4232313890084, 921.480569701145), Loc(-511.4529060472114, 321.3283919667697), Loc(100.70231524185124, 45.25839020895728), Loc(721.8598191969295, -296.8283510996366), Loc(760.8697107496637, -845.9675937265897), Loc(277.74720767349186, -1146.0436825937772), Loc(-685.4970375901803, -941.9919421640898), Loc(-979.571604680024, -653.9188968515897), Loc(-997.5761700120554, -32.76139289651155), Loc(-847.5381255784616, 522.3793715077854), Loc(-829.5335602464299, 1182.5467670156063)],
             "fill_color": MapTheme.INDUSTRY,
             "area_type": "mining industry"
            },
            {"location": Loc(4800, 1500),
             "vertices": [Loc(-455.9388296067805, 133.3521562978235), Loc(-34.11758468490552, -406.78480366311396), Loc(1010.147204572907, -437.6497728037391), Loc(665.4883825025945, 786.6606697743855), Loc(871.2548434400946, 1702.3214209462608), Loc(130.4955840650946, 1872.078751219698), Loc(-810.8859747239678, 1326.7976297353234), Loc(-1294.4371579270928, 431.7135246571984), Loc(-635.984482927093, 421.42520161032326)],
             "fill_color": MapTheme.CITY,
             "area_type": "dense urban area"
            })

        for area in areas:
            new_area = self.spawn_actor(MapViewArea, area["location"])
            new_area.area_type = area["area_type"]
            new_area.vertices = area["vertices"]
            new_area.fill_color = area["fill_color"]
            new_area.drawable_padding = Loc(1000, 1000)

    def __draw_key_areas(self):
        """Draw key areas (area to travel to a side-view world)
        """

        # Each key area has a unique name, used to identify worlds.
        # Values are associated data, used to create worlds.
        key_areas = {
            "Office":
                {"location": Loc(2000, 600)
                },

            "John's Toy Factory":
                {"location": Loc(1000, 2700),
                 #"world_class": GameplayStatics.persistent_world_cache["key_area_classes"]["John's"]
                },

            "Industrial Dump":
                {"location": Loc(300, 4200)
                },

            "City Construction Site":
                {"location": Loc(3400, 2100)
                },

            "Nuclear Power Plant":
                {"location": Loc(900, -200)
                },

            "Nuclear Treatment Facility":
                {"location": Loc(700, 6000)
                },
            }

        last_key_area = GameplayStatics.persistent_world_cache["last_key_area"]
        self.spawned_key_area_actors = {}

        for name, assoc_data in key_areas.items():
            if last_key_area and name == last_key_area:
                # This is where the player came out from.
                self._player_start_location = assoc_data["location"]

            new_area = self.spawn_actor(MapKeyArea, assoc_data["location"])
            new_area.name = name
            self.spawned_key_area_actors[name] = new_area
            #new_area.connecting_world_class = assoc_data["world_class"]

    def __draw_roads(self):
        """Draw roads.
        """

        roads = (
            [Loc(727.2183207838393, 2.1760374501700426), Loc(1117.317236311183, -92.13359047951712), Loc(1485.9821454908692, -87.84678920998749), Loc(1666.0277988111816, -10.684366358424995), Loc(1597.4389784986815, 366.55414536032504), Loc(1640.3069911939942, 752.366259618138), Loc(1987.5378940260252, 1159.6123802236025), Loc(2634.4162055982897, 1419.8212172841509), Loc(3406.0404341139183, 1481.5511555654043), Loc(4187.95298567642, 1795.3450084950923), Loc(3241.427265363919, 1908.5165620107227), Loc(2706.434466926419, 2469.23016806541), Loc(1651.881354621731, 2448.653521971666), Loc(1381.1698544508324, 2738.6556278554685), Loc(1026.2227093336446, 2954.7104118398534), Loc(1118.8176167555193, 3139.9002266836033), Loc(1130.391980183254, 3433.1174335195406), Loc(1037.7970727613788, 3741.7671249257905), Loc(844.8910156324721, 4698.5811682852), Loc(763.8704716383311, 5848.301268773509)],
        )

        for road in roads:
            new_road = self.spawn_actor(MapViewRoad, Loc(0, 0))
            new_road.vertices = road
