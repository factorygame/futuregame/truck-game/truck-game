"""Hud classes for displaying information during gameplay.
"""

from factorygame import Loc, FColor, MathStat, GameplayStatics
from factorygame.core.engine_base import ETickGroup
from factorygame.core.blueprint import DrawnActor


class TruckHud(DrawnActor):
    """Base class of truck game HUDs
    """

    def __init__(self):
        super().__init__()
        self.primary_actor_tick.tick_group = ETickGroup.UI

    def _draw_hint_text(self):
        """Show hint text in top right corned from world.

        World must have hint_text variable declared. Hint text not
        shown if hint_text is empty string or otherwise not True.
        """

        if not self.world.hint_text:
            return

        canvas_size = self.world.get_canvas_dim()
        position = Loc(canvas_size.x - 20, 20)

        font_size = MathStat.clamp(
            int(30 * 1/self.world.get_screen_size_factor()) - 20,
            7, 24)

        self.world.create_text(
            position,
            text="Hint",
            anchor="ne",
            fill=FColor(140).to_hex(),
            font=("Arial", font_size),
            tags=(self.unique_id))

        #font_size += 5
        position.y += int(30 * 1/self.world.get_screen_size_factor()) - 10

        self.world.create_text(
            position,
            text=self.world.hint_text,
            anchor="ne",
            font=("Arial", font_size),
            tags=(self.unique_id))


class KeyAreaHud(TruckHud):
    """Hud shown in Key Areas eg side view truck.
    """

    def _draw(self):
        self._draw_hint_text()


class WorldViewHud(TruckHud):
    """Hud shown in World View eg top down truck.
    """

    def _draw(self):
        self._draw_hint_text()
        self._draw_mission_stats()

    def _draw_mission_stats(self):
        """Draw text for mission if active.
        """

        canvas_size = self.world.get_canvas_dim()
        position = Loc(canvas_size.x / 2, 20)  # center

        font_size = MathStat.clamp(
            int(30 * 1/self.world.get_screen_size_factor()) - 20,
            7, 24)

        line_gap = int(30 * 1/self.world.get_screen_size_factor()) - 10

        # Write destination

        # PLEASE make sure these keys are always set in world cache!
        if GameplayStatics.persistent_world_cache["mission_has_dumped"]:
            # Back to office when completed mission!
            dest_name = "Office"
        elif GameplayStatics.persistent_world_cache["mission_has_loaded"]:
            dest_name = GameplayStatics.persistent_world_cache["mission_dump_area"]
        else:
            dest_name = GameplayStatics.persistent_world_cache["mission_load_area"]

        if dest_name:

            position.x -= 3

            self.world.create_text(
                position,
                text="Destination\nDistance",
                anchor="ne",
                fill=FColor(140).to_hex(),
                font=("Arial", font_size),
                tags=(self.unique_id))

            #font_size += 5
            #position.y += line_gap

            position.x += 6

            target_key_area = self.world.spawned_key_area_actors[dest_name]
            dist = max(abs(self.world.player.location - target_key_area.location) - 120, 0)

            if dest_name == "Office":
                dest_name = "Mission Complete! Head back to office"

            self.world.create_text(
                position,
                text=dest_name + "\n" + str(round(dist)) + " miles",
                anchor="nw",
                font=("Arial", font_size),
                tags=(self.unique_id))
