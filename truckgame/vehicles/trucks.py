"""Data definitions for truck variants.
"""

from math import pi

from factorygame import FColor, Loc, MathStat
from factorygame.core.blueprint import GeomHelper
from truckgame.core.vehicle_base import SideViewVehicle, TopDownVehicle


def _create_arrow_vertices(radius,
                           rotation,
                           length_scale=1.0,
                           arrow_pointiness=0.5,
                           center_forwardness_bias=0.0):
    """Return a tuple of vertices to make an arrow pointing in the direction.

    Arrow is composed of two parts: rectangle and triangle. Triangle is
    attached to the rectangle's short edge to point in the direction of
    rotation.

    Higher arrow pointiness means longer arrow. Range [0, 1]

    Higher length scale means more downwardly stretched body. Range [1, inf)

    Higher center forwardness bias means arrow center will be closer
    tip of triangle rather than back of rectangle. Range [0, 1]
    """

    direction = tuple(GeomHelper.generate_reg_poly(1, radial_offset=rotation + pi/2))[0]

    octagon = tuple(GeomHelper.generate_reg_poly(8, radius=radius, radial_offset=rotation))
    arrow_head = MathStat.lerp(octagon[0], octagon[1], 0.5) + direction * radius * arrow_pointiness

    center_forwardness_bias = MathStat.clamp(center_forwardness_bias)
    forward_displacement = direction * length_scale * (1 - center_forwardness_bias)
    backward_displacement = direction * length_scale * center_forwardness_bias

    return (# Triangle vertices of arrow.
            octagon[0] + forward_displacement,
            arrow_head + forward_displacement,
            octagon[1] + forward_displacement,

            # Bottom vertices of rectangle
            octagon[4] - backward_displacement,
            octagon[5] - backward_displacement)


class SmallGreyTruckSide(SideViewVehicle):
    def begin_play(self):
        super().begin_play()

        self.fill_color = FColor(128)
        self.vertices = [Loc(243.55102040816348, -1.1020408163261948), Loc(293.14285714285734, 120.12244897959215), Loc(177.42857142857167, 157.59183673469414), Loc(114.61224489795939, 331.714285714286), Loc(-27.551020408163026, 337.22448979591866), Loc(-38.57142857142833, 59.51020408163299), Loc(-331.7142857142855, 57.30612244897992), Loc(-328.40816326530586, 3.410605131648481e-13), Loc(-185.14285714285694, -1.1020408163261948), Loc(-200.57142857142833, -23.142857142856798), Loc(-125.63265306122423, -30.85714285714249), Loc(-127.8367346938773, 4.408163265306484), Loc(94.77551020408191, 1.102040816326877), Loc(85.95918367346962, -27.551020408162913), Loc(154.28571428571445, -30.85714285714249), Loc(154.28571428571445, 2.2040816326533843)]


class SmallGreyTruckSideLoaded(SideViewVehicle):
    def begin_play(self):
        super().begin_play()

        self.fill_color = FColor(128)
        self.vertices = [Loc(243.55102040816348, -1.1020408163261948), Loc(293.14285714285734, 120.12244897959215), Loc(177.42857142857167, 157.59183673469414), Loc(114.61224489795939, 331.714285714286), Loc(-27.551020408163026, 337.22448979591866), Loc(-38.57142857142833, 59.51020408163299), Loc(-138.48979591836803, 64.28571428571763), Loc(-120.85714285714357, 153.9183673469421), Loc(-134.81632653061098, 153.55102040816104), Loc(-110.57142857142742, 245.3877551020355), Loc(-242.08163265306007, 250.16326530611715), Loc(-257.87755102040705, 163.83673469387224), Loc(-237.30612244897844, 163.46938775509673), Loc(-233.99999999999844, 58.04081632652341), Loc(-331.7142857142855, 57.30612244897992), Loc(-328.40816326530586, 3.410605131648481e-13), Loc(-185.14285714285694, -1.1020408163261948), Loc(-200.57142857142833, -23.142857142856798), Loc(-125.63265306122423, -30.85714285714249), Loc(-127.8367346938773, 4.408163265306484), Loc(94.77551020408191, 1.102040816326877), Loc(85.95918367346962, -27.551020408162913), Loc(154.28571428571445, -30.85714285714249), Loc(154.28571428571445, 2.2040816326533843)]


class SmallGreyTruckTop(TopDownVehicle):
    def begin_play(self):
        super().begin_play()

        self.fill_color = FColor(128)

    def set_verts_from_rotation(self):
        self.vertices = _create_arrow_vertices(30, self.rotation, length_scale=0.2, center_forwardness_bias=1.0)


class LongRedTruckSide(SideViewVehicle):
    def begin_play(self):
        super().begin_play()

        self.fill_color = FColor(230, 108, 108)
        self.vertices = [Loc(262.28571428571627, 3.6734693877580753), Loc(287.6326530612266, 98.4489795918397), Loc(190.6530612244918, 116.08163265306416), Loc(131.14285714285916, 280.2857142857173), Loc(48.489795918369396, 304.5306122449009), Loc(-20.938775510201936, 299.02040816326826), Loc(-79.34693877550819, 300.1224489795949), Loc(-73.83673469387554, 146.938775510207), Loc(-89.26530612244687, 54.367346938778496), Loc(-159.79591836734494, 49.95918367347235), Loc(-161.99999999999795, 342.00000000000296), Loc(-391.2244897959125, 344.2040816326563), Loc(-570.8571428571371, 325.46938775510534), Loc(-807.7959183673411, 349.71428571428896), Loc(-860.6938775510146, 308.9387755102074), Loc(-862.8979591836676, 57.673469387758416), Loc(-843.06122448979, 8.08163265306456), Loc(-719.6326530612193, 3.673469387758473), Loc(-720.7346938775457, -19.469387755098694), Loc(-654.6122448979539, -16.163265306119115), Loc(-661.2244897959131, 8.08163265306456), Loc(-386.81632653060706, 3.673469387758473), Loc(-369.9183673469339, -27.183673469383734), Loc(-318.48979591836246, -16.163265306118433), Loc(-295.7142857142809, 16.89795918367747), Loc(-40.77551020407873, -3.6734693877516236), Loc(80.4489795918393, -1.4693877550985235), Loc(83.38775510204329, -24.24489795918015), Loc(140.69387755102304, -8.816326530608734), Loc(167.14285714285967, 8.816326530615783)]


class LongRedTruckTop(TopDownVehicle):
    def begin_play(self):
        super().begin_play()

        self.fill_color = FColor(230, 108, 108)

    def set_verts_from_rotation(self):
        self.vertices = _create_arrow_vertices(30, self.rotation, length_scale=120.0, center_forwardness_bias=0.8)
